import numpy as np

class nodo:
    def __init__(self, index, padre, label):
        self.index = index #int
        self.padre = padre #int
        self.hijo = []   #int
        self.label = label #string
        self.value = None
        self.der = None

def funcstr_graph(func, idx = None):
    global i
    global nodes_list
    i+=1
    i_copy = i
    splitted = func.split('+',1)
    if len(splitted) == 2:
        nodes_list.append(nodo(i,idx,'+'))
        if idx!=None:
            nodes_list[idx].hijo.append(i)
        funcstr_graph(splitted[0],i_copy)
        funcstr_graph(splitted[1],i_copy)
    elif len(splitted) == 1:
        splitted = func.split('*',1)
        if len(splitted) == 2:
            nodes_list.append(nodo(i,idx,'*'))
            if idx!=None:
                nodes_list[idx].hijo.append(i)
            funcstr_graph(splitted[0],i_copy)
            funcstr_graph(splitted[1],i_copy)
        else:
            nodes_list.append(nodo(i,idx,splitted[0]))
            if idx!=None:
                nodes_list[idx].hijo.append(i)
    else: print('error')

i = -1
nodes_list = []
funcstr_graph('x*x*y+x+y')



def evaluate(x,y,j=0):
    global nodes_list
    if nodes_list[j].label == '+':
        ret = evaluate(x,y,nodes_list[j].hijo[0]) + evaluate(x,y,nodes_list[j].hijo[1])
    elif nodes_list[j].label == '*':
        ret = evaluate(x,y,nodes_list[j].hijo[0]) * evaluate(x,y,nodes_list[j].hijo[1])
    elif nodes_list[j].label == 'x':
        ret = x
    elif nodes_list[j].label == 'y':
        ret = y
    else: ret = float(nodes_list[j].label)
    nodes_list[j].value = ret
    return ret


evaluate(1,6)

def gradient(x,y,j=0):
    global nodes_list

    if nodes_list[j].padre == None:
        nodes_list[j].der = 1
        for h in nodes_list[j].hijo:
            gradient(x,y,h)
    elif nodes_list[nodes_list[j].padre].label == '+':
        nodes_list[j].der = gradient(x,y,nodes_list[j].padre)
        for h in nodes_list[j].hijo:
            gradient(x,y,h)
    elif nodes_list[nodes_list[j].padre].label == '*':
        if nodes_list[nodes_list[j].padre].hijo[0] == j:
            nodes_list[j].der = nodes_list[nodes_list[nodes_list[j].padre].hijo[1]].value * gradient(x,y,nodes_list[j].padre)
        else: 
            nodes_list[j].der = nodes_list[nodes_list[nodes_list[j].padre].hijo[0]].value * gradient(x,y,nodes_list[j].padre)
        for h in nodes_list[j].hijo:
            gradient(x,y,h)

gradient(1,6)
derx = 0
dery = 0
for j in range(len(nodes_list)):
    if nodes_list[j].label == 'x':
        derx += nodes_list[j].der
    elif nodes_list[j].label == 'y':
        dery += nodes_list[j].der
for i in nodes_list:
    print(f'{i.label}, {i.padre}, {i.hijo}, {i.index}, {i.value} \n')
print(derx,dery)
